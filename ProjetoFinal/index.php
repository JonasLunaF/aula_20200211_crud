<!DOCTYPE html>
<!-- saved from url=(0014)about:internet -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>E-Sports PROJECT</title>

    <link rel="canonical" >

    <!-- Bootstrap core CSS -->
<link href="./Carousel Template · Bootstrap_files/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="icon" type="imagem/png" href="img/icon.png" />

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }

      .times {
        border-radius: 80%;        
      }

      .carousel_1 {
        background-image: url(https://i.imgur.com/kJPDVHT.jpg);
        background-repeat: no-repeat;
        background-size: cover; 
        background-position: center;
      }

      .carousel_2 {
        background-image: url(https://i.imgur.com/qxIufZR.jpg);
        background-repeat: no-repeat;
        background-size: cover; 
        background-position: center;
      }

      .carousel_3 {
        background-image: url(https://pbs.twimg.com/media/D5nVc8ZWwAEsvq_.jpg);
        background-repeat: no-repeat;
        background-size: cover; 
        background-position: center;
      }


    </style>


    <!-- Custom styles for this template -->
    <link href="./Carousel Template · Bootstrap_files/carousel.css" rel="stylesheet">
  </head>
  <body>
    <header>
  <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">

    <div class="collapse navbar-collapse" id="navbarCollapse">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" >E-Sports Project <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item active">
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="login.php" >LOGIN</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="logout.php" >SAIR</a>
        </li>
      </ul>
      <form class="form-inline mt-2 mt-md-0">
      </form>
    </div>
  </nav>
</header>

<main role="main">

  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1" class=""></li>
      <li data-target="#myCarousel" data-slide-to="2" class=""></li>
    </ol>


    <div  class="carousel-inner">

      <div class="carousel-item active">
        <img class="carousel_1">
        <div class="container">
          <div class="carousel-caption text-left">
            <h1>MIBR</h1>
            <p><a class="btn btn-lg btn-primary" href="mibr.php" role="button">Ler Sobre</a></p>
          </div>
        </div>
      </div>


      <div class="carousel-item">
        <img class="carousel_2" >
        <div class="container">
          <div class="carousel-caption">
            <h1>FaZe Clan</h1>
            <p><a class="btn btn-lg btn-primary" href="faze.php" role="button">Ler Sobre</a></p>
          </div>
        </div>
      </div>


      <div class="carousel-item">
        <img class="carousel_3" >
        <div class="container">
          <div class="carousel-caption text-right">
            <h1>DETONA Gaming</h1>
            <p></p>
            <p><a class="btn btn-lg btn-primary" href="detona.php" role="button">Ler Sobre</a></p>
          </div>


        </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Anterior</span>
    </a>
    <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Proximo</span>
    </a>
  </div>


  <!-- Marketing messaging and featurettes
  ================================================== -->
  <!-- Wrap the rest of the page in another container to center all the content. -->

  <div class="container marketing">

    <!-- Three columns of text below the carousel -->
    <div class="row">
      <div class="col-lg-4">
        <img class="times" src="img/fazemi.jpg" width="200" height="200">
        <h2>FaZe Clan</h2>
        <p>O super time original do CS: GO, Galácticos ou apenas FaZe Clan. Uma equipe composta por cinco jogadores de cinco países diferentes, que também estão entre os melhores jogadores do mundo, faz do FaZe Clan um verdadeiro candidato ao primeiro lugar no mundo. Vitórias maciças incluem corridas invictas em vários torneios premier, e não há como negar que o FaZe é sempre um dos principais favoritos para ganhar em um torneio BLAST Pro Series.</p>
        <p><a href="faze.php" class="btn btn-secondary" href="#" role="button">Ver Mais »</a></p>
      </div><!-- /.col-lg-4 -->

      <div class="col-lg-4">
        <img class="times" src="img/flamengo.jpg" width="200" height="200">
        <h2>Flamengo eSports</h2>
        <p>Campeão do Campeonato Brasileiro de League of Legends (CBLoL) em 2019, o Rubro-Negro precisou começar de baixo no cenário, desde o Circuito Desafiante, a segunda divisão do LoL nacional, até a elite, passando por diversos testes para se provar como uma grande equipe.</p>
        <p><a class="btn btn-secondary" href="flamengo.php" role="button">Ver Mais »</a></p>
      </div><!-- /.col-lg-4 -->

      <div class="col-lg-4">
        <img class="times" src="img/mibr.jpg" width="200" height="200">
        <h2>MIBR</h2>
        <p>MIBR, também conhecida como Made In Brazil, é uma organização brasileira de esportes eletrônicos fundada em 2003 e que encerrou as atividades em 2012.A organização quase retornou em 2016, porém o dono da marca, Paulo Velloso, afirmou que o time só voltaria "quando houver previsibilidade de custos e faturamento".</p>
        <p><a href="mibr.php" class="btn btn-secondary" href="#" role="button">Ver Mais »</a></p>
      </div><!-- /.col-lg-4 -->
    </div><!-- /.row -->


    <!-- START THE FEATURETTES -->

    <hr class="featurette-divider">

    <div class="row featurette">
      <div class="col-md-7">
        <h2 class="featurette-heading">Kabum eSports. <span class="text-muted"></span></h2>
        <p class="lead">Desde 2013, o KaBuM! eSports investe em seu projeto de League of Legends e na evolução do cenário. A estrutura oferecida pela organização envolve Gaming House com equipamentos de última geração e o suporte necessário para que os jogadores possam se aprimorar e obter resultados bem sucedidos. Uma das equipes brasileiras de eSports a se classificar e disputar o Worlds.</p>
      </div>
      <div class="col-md-5">
        <img class="times" height="100%" width="100%" src="img/kabum.png">
      </div>
    </div>

    <hr class="featurette-divider">

    <div class="row featurette">
      <div class="col-md-7 order-md-2">
        <h2 class="featurette-heading">DETONA Gaming <span class="text-muted"></span></h2>
        <p class="lead">Detona Gaming é uma organização fundada pelos jogadores CSR*, tiburci0 e também DJ Perera com intuito de alcançar uma grande marca no cenário competitivo de CS:GO Com pouco mais 6 meses de existência a equipe já alcançou a liga principal da GamersClub e vem tomando conta da alegria de todos por conta de seus dois fundadores que são youtubers de sucesso e levam a Detona a um time de grande reconhecimento nacional.</p>
      </div>
      <div class="col-md-5 order-md-1">
        <img width="100%" height="100%" class="times" src="img/detona.jpg">
      </div>
    </div>

    <hr class="featurette-divider">

    <div class="row featurette">
      <div class="col-md-7">
        <h2 class="featurette-heading">Cloud9 <span class="text-muted"></span></h2>
        <p class="lead">Cloud9 é uma organização americana de esports profissional sediada em Los Angeles , Califórnia . Foi formado em 2013, quando o CEO Jack Etienne comprou a antiga lista da Quantic Gaming League of Legends . Após o sucesso da equipe de League of Legends da Cloud9 na Série Norte-Americana de League of Legends , a equipe começou a se expandir para outros esportes.</p>
      </div>
      <div class="col-md-5">
        <img class="times" width="100%" height="100%" src="img/cloud9.jpg">
      </div>
    </div>

    <hr class="featurette-divider">

    <!-- /END THE FEATURETTES -->

  </div><!-- /.container -->


  <!-- FOOTER -->

<?php include 'footer2.php' ?>

</main>
<script src="./Carousel Template · Bootstrap_files/jquery-3.3.1.slim.min.js.download" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
      <script>window.jQuery || document.write('<script src="/docs/4.3/assets/js/vendor/jquery-slim.min.js"><\/script>')</script><script src="./Carousel Template · Bootstrap_files/bootstrap.bundle.min.js.download" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script>

</body></html>