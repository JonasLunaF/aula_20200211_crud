<?php
session_start();

include 'conexao.php';

if(empty($_SESSION["login"])){
  echo "<script>alert('Faça o login primeiramente!')</script>";
  header("Location:login.php");
}
?>

<!DOCTYPE html>
<!-- saved from url=(0014)about:internet -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Detona E-Sports</title>
<link rel="icon" type="imagem/png" href="img/icon.png" />
    <link rel="canonical" href="https://getbootstrap.com/docs/4.3/examples/blog/">

    <!-- Bootstrap core CSS -->
<link href="./Blog Template · Bootstrap_files/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


<style>
.bd-placeholder-img {
  font-size: 1.125rem;
  text-anchor: middle;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

@media (min-width: 768px) {
  .bd-placeholder-img-lg {
    font-size: 3.5rem;
  }
}

table, th, td {
border: 1px solid black;
}

</style>
    <!-- Custom styles for this template -->
    <link href="./Blog Template · Bootstrap_files/css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="./Blog Template · Bootstrap_files/blog.css" rel="stylesheet">
  </head>
  <body>
    <div class="container">
  <header class="blog-header py-3">
    <div class="row flex-nowrap justify-content-between align-items-center">
      <div class="col-4 pt-1">
        <a class="text-muted" href=""></a>
      </div>
      <div class="col-4 text-center">
        <a class="blog-header-logo text-dark" href="index.php">E-Sports Brasil</a>
      </div>
      <div class="col-4 d-flex justify-content-end align-items-center">
        <a class="text-muted" href="   ">
          
        </a>
      </div>
    </div>
  </header>

  <div class="jumbotron p-4 p-md-5 text-white rounded bg-dark">
    <div class="col-md-6 px-0">
      <h1 class="display-4 font-italic">Detona Gaming</h1>
      <p class="lead my-3">DETONA Gaming é uma organização brasileira patrocinada pelo cantor brasileito de funk Dj Perera.</p>
    </div>
  </div>

  <div class="row mb-2">

    <div class="col-md-6">
      <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
        <div class="col p-4 d-flex flex-column position-static">
          <h3 class="mb-0">Vinicius (v$m)</h3>
          <p class="card-text mb-auto">Vinicius (v$m) é um jogador brasileiro de CS:GO que atualmente joga pelo DETONA. O mesmo não pode participar de eventos Valve devido a uma proibição anterior do VAC que ocorreu em novembro de 2018.</p>
        </div>
        <div class="col-auto d-none d-lg-block">
          <img width="250px" height="250px" src="https://static.hltv.org/images/playerprofile/thumb/16816/800.jpeg?v=4">
        </div>
      </div>
    </div>

    <div class="col-md-6">
      <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
        <div class="col p-4 d-flex flex-column position-static">
          <h3 class="mb-0">André (Tiburci0)</h3>
          <p class="mb-auto">André (Tiburci0) é um jogador profissional brasileiro de CS:GO e CEO da DETONA Gaming.</p>
        </div>
        <div class="col-auto d-none d-lg-block">
          <img height="250px" width="250px" src="https://pbs.twimg.com/profile_images/1186421788303081478/jZabHNPL_400x400.jpg">
        </div>
      </div>
    </div>

  </div>

<main role="main" class="container">
  <div class="row">
    <div class="col-md-8 blog-main">
      <h3 class="pb-4 mb-4 font-italic border-bottom">
        #GODETONA
      </h3>

      <div class="blog-post">
        <h2 class="blog-post-title">HISTORIA</h2>

        <hr>
        <p>Detona Gaming é uma organização fundada pelos jogadores CSR*, tiburci0 e também DJ Perera com intuito de alcançar uma grande marca no cenário competitivo de CS:GO Com pouco mais 6 meses de existência a equipe já alcançou a liga principal da GamersClub e vem tomando conta da alegria de todos por conta de seus dois fundadores que são youtubers de sucesso e levam a Detona a um time de grande reconhecimento nacional.</p>
        <blockquote>
          
        <h2></h2>
        <p></p>

        <h3></h3>
        <p></p>

        <h3></h3>
        <p></p>

      <div class="blog-post">
        <h2 class="blog-post-title"></h2>
      </div><!-- /.blog-post -->

</main><!-- /.container -->

<?php include 'footer.php' ?>

</body></html>