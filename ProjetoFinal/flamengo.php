<?php
session_start();

include 'conexao.php';

if(empty($_SESSION["login"])){
  echo "<script>alert('Faça o login primeiramente!')</script>";
  header("Location:login.php");
}
?>

<!DOCTYPE html>
<!-- saved from url=(0014)about:internet -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Flamengo E- Sports</title>
<link rel="icon" type="imagem/png" href="img/icon.png" />
    <link rel="canonical" href="https://getbootstrap.com/docs/4.3/examples/blog/">

    <!-- Bootstrap core CSS -->
<link href="./Blog Template · Bootstrap_files/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


<style>
.bd-placeholder-img {
  font-size: 1.125rem;
  text-anchor: middle;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

@media (min-width: 768px) {
  .bd-placeholder-img-lg {
    font-size: 3.5rem;
  }
}

table, th, td {
border: 1px solid black;
}

</style>
    <!-- Custom styles for this template -->
    <link href="./Blog Template · Bootstrap_files/css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="./Blog Template · Bootstrap_files/blog.css" rel="stylesheet">
  </head>
  <body>
    <div class="container">
  <header class="blog-header py-3">
    <div class="row flex-nowrap justify-content-between align-items-center">
      <div class="col-4 pt-1">
      </div>
      <div class="col-4 text-center">
        <a class="blog-header-logo text-dark" href="index.php">E-Sports TIMES</a>
      </div>
      <div class="col-4 d-flex justify-content-end align-items-center">
        <a class="text-muted" href="   ">
        </a>
      </div>
    </div>
  </header>

  <div class="jumbotron p-4 p-md-5 text-white rounded bg-dark">
    <div class="col-md-6 px-0">
      <h1 class="display-4 font-italic">Flamengo E-SPORTS</h1>
      <p class="lead my-3">O Flamengo eSports é um time brasileiro, a divisão de e-sports da tradicional equipe de esportes CR Flamengo.</p>
    </div>
  </div>

  <div class="row mb-2">

    <div class="col-md-6">
      <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
        <div class="col p-4 d-flex flex-column position-static">
          <h3 class="mb-0">Felipe(Brtt)</h3>
          <p class="card-text mb-auto">É um profissional brasileiro em Leugue Of Legends com a posiçao de atirador um dos melhores do Brasil
          Brtt ja foi considerado por varios anos seguidos o melhor ADC/jogador do brasil.</p>
        </div>
        <div class="col-auto d-none d-lg-block">
          <img height="250px" width="250px" src="https://s2.glbimg.com/6macObLaCw25y75jwHTslxkVeHE=/0x0:524x508/924x0/smart/filters:strip_icc()/i.s3.glbimg.com/v1/AUTH_bc8228b6673f488aa253bbcb03c80ec5/internal_photos/bs/2019/z/m/CvvKRqTvaQoMzu43CawA/brtt-tatuagem.jpg">
        </div>
      </div>
    </div>

    <div class="col-md-6">
      <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
        <div class="col p-4 d-flex flex-column position-static">
          <h3 class="mb-0">Lee(Shirimp)</h3>
          <p class="mb-auto">"Shrimp" Lee Byeong-hoon é um jogador jogador sul-coreano de League of Legends, atualmente o Caçador da Flamengo eSports.</p>
        </div>
        <div class="col-auto d-none d-lg-block">
          <img height="250px" width="250px" src="https://pt.esportspedia.com/lol/images/thumb/f/f0/FLA_Shrimp_2019_Spring.jpg/300px-FLA_Shrimp_2019_Spring.jpg">
        </div>
      </div>
    </div>

  </div>

<main role="main" class="container">
  <div class="row">
    <div class="col-md-8 blog-main">
      <h3 class="pb-4 mb-4 font-italic border-bottom">
        #GOFLA
      </h3>

      <div class="blog-post">
        <h2 class="blog-post-title">HISTORIA</h2>

        <hr>
        <p>
        A equipe do Flamengo eSports foi criada no final de 2017 como uma filial do tradicional clube de futebol CR Flamengo. A lista inicial incluía Jisu, SirT, Evrot, BrTT e EsA, e fez sua primeira aparição no BRCC 2018 Split 1. Como vice-campeões, eles se classificaram para a Access Series e chegaram à primeira divisão do Brasil ao conquistar o Team oNe eSports. A equipe de eSports herdou muitos torcedores de seu clube de futebol, e seu investimento inicial foi frequentemente reconhecido pelos lançadores da CBLOL como um grande passo para os eSports no Brasil.

        Os jogadores e torcedores do Flamengo geralmente se referem a si mesmos como "nação rubro-negra".</p>
          
        <h2>CAMPEONATO BRASILEIRO</h2>
        <p>É campeão! No último sábado , o Flamengo eSports levantou o título do Campeonato Brasileiro de League of Legends. Contando com o apoio da Nação, que lotou a Jeunesse Arena e fez uma linda festa, o Mais Querido venceu a INTZ por 3 a 2 em uma série emocionante do início ao fim. Com a conquista, o Mengão garantiu a vaga no Mundial e será o representante brasileiro. 

        O Flamengo utilizou durante todas as partidas o seu quinteto titular. Robô (Topo), Shrimp (Caçador), Goku (Meio), brTT (Atirador) e Luci (Suporte). No primeiro jogo, que contou com a vitória da INTZ, o Flamengo não conseguiu se encontrar. A equipe adversária neutralizou bem o time rubro-negro e impediu que o FLA desenvolvesse o seu jogo. Com um bom trabalho de Shini, a INTZ jogou nos erros do Mengão e levou o game.</p>

      <div class="blog-post">
        <h2 class="blog-post-title"> </h2>
      </div><!-- /.blog-post -->

</main><!-- /.container -->

<?php include 'footer.php'; ?>

</body></html>