<?php
session_start();

include 'conexao.php';

if(empty($_SESSION["login"])){
  echo "<script>alert('Faça o login primeiramente!')</script>";
  header("Location:login.php");
}
?>

<!DOCTYPE html>
<!-- saved from url=(0014)about:internet -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>MIBR E-Sports</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.3/examples/blog/">

    <!-- Bootstrap core CSS -->
<link href="./Blog Template · Bootstrap_files/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


<style>
.bd-placeholder-img {
  font-size: 1.125rem;
  text-anchor: middle;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

@media (min-width: 768px) {
  .bd-placeholder-img-lg {
    font-size: 3.5rem;
  }
}

table, th, td {
border: 1px solid black;
}

</style>
    <!-- Custom styles for this template -->
    <link href="./Blog Template · Bootstrap_files/css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="./Blog Template · Bootstrap_files/blog.css" rel="stylesheet">
    <link rel="icon" type="imagem/png" href="img/icon.png" />
  </head>
  <body>
    <div class="container">
  <header class="blog-header py-3">
    <div class="row flex-nowrap justify-content-between align-items-center">
      <div class="col-4 pt-1">
      </div>
      <div class="col-4 text-center">
        <a class="blog-header-logo text-dark" href="index.php">E-Sports TIMES</a>
      </div>
      <div class="col-4 d-flex justify-content-end align-items-center">
        <a class="text-muted" href="   ">
        </a>
      </div>
    </div>
  </header>

  <div class="jumbotron p-4 p-md-5 text-white rounded bg-dark">
    <div class="col-md-6 px-0">
      <h1 class="display-4 font-italic">MIBR</h1>
      <p class="lead my-3">MIBR, também conhecida como Made In Brazil, é uma organização brasileira de esportes eletrônicos fundada em 2003 e que encerrou as atividades em 2012</p>
    </div>
  </div>

  <div class="row mb-2">

    <div class="col-md-6">
      <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
        <div class="col p-4 d-flex flex-column position-static">
          <h3 class="mb-0">Gabriel (Fallen)</h3>
          <p class="card-text mb-auto">Gabriel Toledo de Alcântara Sguário, mais conhecido como FalleN, é um jogador profissional de Counter-Strike: Global Offensive que joga atualmente pela MIBR.</p>
        </div>
        <div class="col-auto d-none d-lg-block">
          <img width="250px" height="250px" src="https://pbs.twimg.com/profile_images/776630578057711616/z_0b5sIl_400x400.jpg">
        </div>
      </div>
    </div>

    <div class="col-md-6">
      <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
        <div class="col p-4 d-flex flex-column position-static">
          <h3 class="mb-0">Fernando (FER)</h3>
          <p class="mb-auto">Fernando “fer” Alvarenga é um pro player brasileiro de Counter-Strike: Global offensive (CS:GO). </p>
        </div>
        <div class="col-auto d-none d-lg-block">
          <img width="250px" height="250px" src="https://ggscore.com/media/logo/p3931.png">
        </div>
      </div>
    </div>

  </div>

<main role="main" class="container">
  <div class="row">
    <div class="col-md-8 blog-main">
      <h3 class="pb-4 mb-4 font-italic border-bottom">
        #GOMIBR
      </h3>

      <div class="blog-post">
        <h2 class="blog-post-title">HISTORIA</h2>

        <hr>
        <p>MIBR, também conhecida como Made In Brazil, é uma organização brasileira de esportes eletrônicos fundada em 2003 e que encerrou as atividades em 2012. A organização quase retornou em 2016, porém o dono da marca, Paulo Velloso, afirmou que o time só voltaria "quando houver previsibilidade de custos e faturamento". Em 2018, a marca MIBR foi adquirida pela organização americana de esportes eletrônicos Immortals (atualmente Immortals Gaming Club).</p>
        <blockquote>
          
        <h2>ORGANIZAÇÃO</h2>
        <p>A Made in Brazil foi fundada em 2003 no Rio de Janeiro quando o empresário Paulo Velloso decidiu investir na equipe de Counter-Strike do seu filho Rafael "pred" Velloso. O investimento de Paulo elevou o nível da equipe e a levou a se tornar a melhor equipe do Brasil atraindo a atenção do mundo para o CS brasileiro, consolidando o país como uma das potências do esporte eletrônico, se tornando o único time brasileiro campeão da Electronic Sports World Cup (em 2006), competição que era considerada a Copa do Mundo de esportes eletrônicos na época, além de outros títulos importantes como a shgOpen 2007, a DreamHack Winter 2007 e a GameGune 2008.</p>
        <h3>FIM DAS ATIVIDADES</h3>
        <p>Em 14 de março de 2012 a organização anunciou o fim das atividades-<br>
        "Gostaríamos de agradecer a toda a comunidade que nos acompanhou durante esses anos, que reconheceu o MIBR como um time de excelência, significado de vitória. Nem sempre isso foi possível, e nossa torcida esteve presente nos piores momentos pelos quais passamos, seja criticando ou apoiando. Reconhecíamos as críticas como uma demonstração de carinho por aquela que é a maior organização de Counter-Strike que o Brasil já teve. Os elogios serviam para mostrar que estávamos no caminho certo e que o trabalho estava produzindo resultados satisfatórios.
        Quantas noites em claro para assistir o MIBR jogar, para ver os brasileiros representando as cores verde-e-amarelo contra as principais equipes do mundo.
        Ao público que criticou, elogiou, sugeriu, enfim... verdadeiramente torceu, o nosso muito obrigado.
        Não desistimos, não falimos, não perdemos. É apenas o fim de um ciclo. Um ciclo vitorioso. Mas pode não ser um adeus, talvez somente um até logo."</p>
        <h3>RETORNO</h3>
        <p>Em 7 de junho de 2018, Noah Whinston, então CEO da Immortals, anunciou o retorno da marca, agora como propriedade da organização americana (que já havia tido lineups brasileiras desde 2016) com os ex-jogadores da SK Gaming. Pouco mais tarde, no dia 23 de junho, ocorreu um evento onde foi revelado os jogadores de Counter-Strike: Global Offensive da organização.</p>

      <div class="blog-post">
        <h2 class="blog-post-title">Torneios Notáveis</h2>

        <table style="width:100%">
        <tr>
          <th>Colocação</th>
          <th>Data</th>
          <th>Torneio</th>
          <th>Fase</th>
          <th>Resultado</th>
          <th>Premiação</th>
        </tr>
        <tr>
          <td><p>1º</p></td>
          <td><p>23/11/2008</p></td>
          <td><p>IEM III American Championship Finals</p></td>
          <td><p>Final</p></td>
          <td><p>16 - 10</p></td>
          <td><p>$25,000</p></td>
        </tr>
        <tr>
          <td><p> 3º</p></td>
            <td><p>19/10/2008 </p></td>
            <td><p>IEM III Global Challenge Montreal </p></td>
            <td><p>Final da repescagem</p></td>
            <td><p>6 - 16</p></td>
            <td><p>$6,000</p></td>
        </tr>
        <tr>
          <td><p>2º</p></td>
          <td><p>05/10/2008</p></td>
          <td><p>IEM III Global Challenge Los Angeles</p></td>
          <td><p>Final</p></td>
          <td><p>0 : 2</p></td>
          <td><p>$10,000</p></td>
        </tr>
        <tr>
          <td><p>1º</p></td>
          <td><p>23/11/2008</p></td>
          <td><p>IEM III American Championship Finals</p></td><td><p>Final</p></td>
          <td><p>16 - 10</p></td>
          <td><p>$25,000</p></td>
        </tr>
        <tr>
        <td><p>3º</p></td>
        <td><p>19/10/2008</p></td>
        <td><p>IEM III Global Challenge Montreal</p></td>
        <td><p>Final da repescagem</p></td>
        <td><p>6 - 16</p></td>
        <td><p>$6,000</p></td>
        </tr>
        <tr>
        <td><p>2º</p></td>
        <td><p>05/10/2008</p></td>
        <td><p>IEM III Global Challenge Los Angeles</p></td>
        <td><p>Final</p></td>
        <td><p>0 - 2</p></td>
        <td><p>$10,000</p></td>
        </tr>
        <tr>
        <td><p>1º</p></td>
        <td><p>26/07/2008</p></td>
        <td><p>GameGune 2008</p></td>
        <td><p>Final</p></td>
        <td><p>2 - 1</p></td>
        <td><p>$18,806.42</p></td>
        </tr>
        <tr>
        <td><p>2º</p></td>
        <td><p>29/03/2008</p></td>
        <td><p>DreamHack Skellefteå</p></td>
        <td><p>Final</p></td>
        <td><p>0 - 2</p></td>
        <td><p>$0</p></td>
        </tr>
        <tr>
        <td><p>1º</p></td>
        <td><p>02/12/2007</p></td>
        <td><p>DreamHack Winter 2007</p></td>
        <td><p>Final</p></td>
        <td><p>16 - 11</p></td>
        <td><p>$17,215.48</p></td>
        </tr>
        <tr>
        <td><p>4º</p></td>
        <td><p>21/10/2007</p></td>
        <td><p>IEM II Global Challenge Los Angeles</p></td>
        <td><p>Final da repescagem</p></td>
        <td><p>8 - 16</p></td>
        <td><p>$3,000</p></td>
        </tr>
        <tr>
        <td><p>3º</p></td>
        <td><p>10/08/2007</p></td>
        <td><p>WEG e-Stars 2007</p></td>
        <td><p>Fase de grupos</p></td>
        <td><p>1 V 2 E</p></td>
        <td><p>$5,000</p></td>
        </tr>
        <tr>
        <td><p>4º</p></td>
        <td><p>08/07/2007</p></td>
        <td><p>ESWC 2007</p></td>
        <td><p>Decisão do Terceiro lugar</p></td>
        <td><p>0 - 2</p></td>
        <td><p>$0</p></td></tr>
        <tr>
        <td><p>1º</p></td>
        <td><p>12/02/2007</p></td>
        <td><p>shgOpen 2007</p></td>
        <td><p>Final</p></td>
        <td><p>2 - 0</p></td>
        <td><p>$21,146.31</p></td></tr>
        <tr>
        <td><p>2º</p></td>
        <td><p>19/11/2006</p></td>
        <td><p>2006 CPL Brazil</p></td>
        <td><p>Final</p></td>
        <td><p>0 - 2</p></td>
        <td><p>$4,623</p></td></tr>
        <tr>
        <td><p>4º</p></td>
        <td><p>29/09/2006</p></td>
        <td><p>KODE5 2006</p></td>
        <td><p>Decisão do Terceiro lugar</p></td>
        <td><p>10 - 16</p></td>
        <td><p>$0</p></td></tr>
        <tr>
        <td><p>1º</p></td>
        <td><p>02/07/2006</p></td>
        <td><p>ESWC 2006</p></td>
        <td><p>Final</p></td>
        <td><p>16 - 6</p></td>
        <td><p>$52,000</p></td>
        </tr>
        <tr>
        <td><p>4º</p></td>
        <td><p>20/06/2006</p></td>
        <td><p>WSVG DreamHack Summer 2006</p></td>
        <td><p>Semi-Final da repescagem</p></td>
        <td><p>2 - 16</p></td>
        <td><p>$0</p></td>
        </tr>
        <tr>
        <td><p>1º</p></td>
        <td><p>30/10/2005</p></td>
        <td><p>2005 CPL Chile</p></td>
        <td><p>Final</p></td>
        <td><p>16 - 8</p></td>
        <td><p>$5,000</p></td>
        </tr>
        <tr>
        <td><p>1º</p></td>
        <td><p>30/05/2005</p></td>
        <td><p>2005 CPL Brazil</p></td>
        <td><p>Final</p></td>
        <td><p>16 - 9</p></td>
        <td><p>$12,000</p></td>
        </tr>

        </table>

      </div><!-- /.blog-post -->

</main><!-- /.container -->

<?php include 'footer.php' ?>

</body></html>