<?php
session_start();

include 'conexao.php';

if(empty($_SESSION["login"])){
  echo "<script>alert('Faça o login primeiramente!')</script>";
  header("Location:login.php");
}
?>

<!DOCTYPE html>
<!-- saved from url=(0014)about:internet -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="icon" type="imagem/png" href="img/icon.png" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Cloud9 E-Sports</title>

    <!-- Bootstrap core CSS -->
    <link href="./Blog Template · Bootstrap_files/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


<style>
.bd-placeholder-img {
  font-size: 1.125rem;
  text-anchor: middle;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

@media (min-width: 768px) {
  .bd-placeholder-img-lg {
    font-size: 3.5rem;
  }
}

table, th, td {
border: 1px solid black;
}

</style>
    <!-- Custom styles for this template -->
    <link href="./Blog Template · Bootstrap_files/css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="./Blog Template · Bootstrap_files/blog.css" rel="stylesheet">
  </head>
  <body>
    <div class="container">
  <header class="blog-header py-3">
    <div class="row flex-nowrap justify-content-between align-items-center">
      <div class="col-4 pt-1">

      </div>
      <div class="col-4 text-center">
        <a class="blog-header-logo text-dark" href="index.php">E-Sports TIMES</a>
      </div>
      <div class="col-4 d-flex justify-content-end align-items-center">
        <a class="text-muted" href="   ">
        </a>
      </div>
    </div>
  </header>

  <div class="jumbotron p-4 p-md-5 text-white rounded bg-dark">
    <div class="col-md-6 px-0">
      <h1 class="display-4 font-italic">Cloud9</h1>
      <div class="mb-1 text-muted">(Counter-Strike: Ofensiva Global)</div>
      <p class="lead my-3">Cloud9(C9) é uma organização americana de esports profissional sediada em Los Angeles, Califórnia. Foi formado em 2013, quando o CEO Jack Etienne comprou a lista de jogadores do Quantic Gaming League of Legends.</p>
    </div>
  </div>

  <div class="row mb-2">

    <div class="col-md-6">
      <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
        <div class="col p-4 d-flex flex-column position-static">
          <h3 class="mb-0">Timothy (AUTIMATIC)</h3>
          <p class="card-text mb-auto">Timothy (autimatic) ta é um jogador norte-americano de Counter-Strike:Global Offensive e ex-jogador de Counter-Strike:Source. Atualmente, ele joga no Cloud9.</p>
        </div>
        <div class="col-auto d-none d-lg-block">
          <img width="250px" height="250px" src="http://www.hayshighindians.com/Academics/StudentProjects/Projects/Dixon%20Parker%20Website/800%20(2).jpeg">
        </div>
      </div>
    </div>

    <div class="col-md-6">
      <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
        <div class="col p-4 d-flex flex-column position-static">
          <h3 class="mb-0">Damian (daps)</h3>
          <p class="mb-auto">Damian (daps) é um profissional canadense de Counter-Strike:Global Offensive, de ascendência argentina. Atualmente, ele joga no Cloud9.</p>
        </div>
        <div class="col-auto d-none d-lg-block">
          <img width="250px" height="250px" src="https://static.hltv.org/images/playerprofile/thumb/8521/800.jpeg?v=8">
        </div>
      </div>
    </div>

  </div>

<main role="main" class="container">
  <div class="row">
    <div class="col-md-8 blog-main">
      <h3 class="pb-4 mb-4 font-italic border-bottom">
        #GOCloud9
      </h3>

      <div class="blog-post">
        <h2 class="blog-post-title">HISTORIA</h2>

        <hr>
        <p>Cloud9 adquiriu compLexity Gaming 's Counter-Strike: Global Offensive equipe em agosto de 2014. A equipe deixou compLexity depois de ter recebido uma oferta melhor de C9 antes de renovar seu contrato com sua equipe anterior. Depois de passar 2-0 no grupo D da fase de grupos da ESL One; Em Colônia de 2014, o Cloud9 perdeu para o time sueco Ninjas de pijama nas quartas de final, que mais tarde venceu o torneio. Em 26 de novembro, Sean Gares assumiu a posição de líder no jogo, anteriormente pertencente a Spencer 'Hiko' Martin. Em 14 de dezembro, Hiko deixou o Cloud9, para ser substituído por Shahzeb 'ShahZaM' Khan.<br><br>

        Em 24 de abril, a Cloud9 lançou ShahZam e Kory 'Semphis' Friesen. Em 29 de abril, Ryan 'fREAKAZOiD' Abadir e Tyler 'Skadoodle' Latham, ex-iBUYPOWER, juntaram-se à equipe e Braxton 'swag' Pierce ingressou como analista, mais tarde mudou para a posição CS: GO Streamer.<br><br>

        Em 24 de novembro, Sean "sgares" Gares saiu da lista de Counter-Strike.<br><br>

        O Cloud9 tem sido historicamente considerado um dos melhores times norte-americanos do CS: GO, terminando em 2º lugar em vários eventos da LAN contra os principais times europeus, como o FNATIC nas finais da ESEA ESL Pro League.<br><br>

        Cloud9 ficou em 13 a 16 na MLG Columbus 2016, perdendo para Natus Vincere e G2 Esports na fase de grupos.<br><br>

        Em 12 de abril de 2016 anunciou a saída do fREAKAZOiD da lista de partida. Eric "adreN" Hoag, do Team Liquid, foi anunciado como substituto temporário. Alec "Slemmy" White foi anunciado como substituto oficial em 23 de abril de 2016.<br><br>

        O gerente da divisão de Counter-Strike da Cloud9, Tres "stunna" Saranthus deixou a equipe em 26 de julho de 2016. Timothy "autimatic" Ta ingressou na equipe em 17 de agosto de 2016, substituindo Slemmy.<br><br>

        Em 30 de outubro de 2016, o Cloud9 derrotou o SK Gaming 2–1 no melhor de 3 e venceu as finais da 4ª Temporada da ESL Pro League em São Paulo, Brasil.<br><br>

        Em 15 de agosto de 2017, Michael "encobriu" Grzesiek deixou o time, deixando o jogo competitivo de Counter-Strike, anunciando que se tornaria um streamer em tempo integral.<br><br>

        Em 28 de janeiro de 2018, o Cloud9 derrotou o FaZe Clan 2–1 e venceu o ELEAGUE Major: Boston 2018.[82] Ao fazer isso, eles se tornaram o primeiro time norte-americano a ganhar um CS: GO Major.<br><br>

        Em 31 de março de 2018, o principal AWPer do Cloud9, Tyler "Skadoodle" Latham anunciou no Twitter que se tornaria inativo na cena profissional do CS. No mesmo dia, ocorreu uma transferência muito esperada na lista ativa do Cloud9, com Jacky "Stewie2K" Yip encerrando seu contrato de vários anos com o Cloud9 para se mudar para a SK Gaming.<br><br>

        Em 18 de abril de 2018, Michael "encobriu" Grzesiek se aposentou oficialmente do Counter-Strike competitivo e deixou o Cloud9.</p>
         
        <h2>ORGANIZAÇÃO</h2>
        <p>O Cloud9 tem sua origem na equipe de Orbit Gaming, da League of Legends, que tinha vários membros atuais da equipe C9. Após o Lone Star Clash em novembro de 2012, os membros da Orbit Gaming assinaram com a Quantic Gaming, que patrocinava apenas uma equipe de StarCraft II. A Quantic Gaming era uma equipe de esports e empresa de mídia fundada em 2010 por Simon Boudreault, um nativo de Quebec que teve uma grande herança após a morte de seu pai e decidiu investir quase tudo em esports. Durante a sua existência, vários jogadores e treinadores alegaram que constantemente perdiam pagamentos de Boudreault. Quando a QG falhou na separação da primavera de 2013 do LCS, Boudreault interrompeu o contato com a sociedade e logo dissolveu a empresa. Apesar de receber dezenas de milhares de dólares, ex-jogadores dizem que não estão mais considerando uma ação legal contra Boudreault. O ex- gerente da Team SoloMid, Jack Etienne, comprou a equipe por US $ 15.000 em maio de 2013 e também se tornou seu gerente.<br><br>

        Em julho de 2014, a equipe do C9 Smite desapareceu pouco antes do início da Smite Pro League. Em 6 de maio de 2014, Cloud9 contratou o jogador Super Smash Bros. Melee Mang0. Cloud9 adquiriu compLexity Gaming 's Counter-Strike: Global Offensive equipe em agosto de 2014. A equipe deixou compLexity depois de ter recebido uma oferta melhor de C9 antes de renovar seu contrato com sua equipe anterior. Cloud9 anunciou a formação de uma equipe da Challenger Series e realizou testes abertos. Em 26 de novembro de 2014, Cloud9 adicionou um Halocom a aquisição da agência, que ostentava vários jogadores experientes. Em dezembro de 2014, a Cloud9 retirou seu time do Dota 2 da I-League chinesa por causa de preocupações com o mau jogo e as condições de vida e, posteriormente, foi banido pelas próximas temporadas por isso. Em fevereiro de 2015, a Riot suspendeu o C9 Tempest depois que foi revelado que a equipe havia usado ilegalmente um membro que não fazia parte da lista durante um jogo. O nome dele era Joe Fuller.</p>

        <h3></h3>
        <p></p>
        <h3></h3>
        <p></p>

    </div>

      <div class="blog-post">
      </div><!-- /.blog-post -->

</main><!-- /.container -->

<?php include 'footer.php' ?>

</body></html>