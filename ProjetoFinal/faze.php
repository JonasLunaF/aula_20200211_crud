<?php
session_start();

include 'conexao.php';

if(empty($_SESSION["login"])){
  echo "<script>alert('Faça o login primeiramente!')</script>";
  header("Location:login.php");
}
?>

<!DOCTYPE html>
<!-- saved from url=(0014)about:internet -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>FaZe E- Sports</title>
<link rel="icon" type="imagem/png" href="img/icon.png" />
    <link rel="canonical" href="https://getbootstrap.com/docs/4.3/examples/blog/">

    <!-- Bootstrap core CSS -->
<link href="./Blog Template · Bootstrap_files/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


<style>
.bd-placeholder-img {
  font-size: 1.125rem;
  text-anchor: middle;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

@media (min-width: 768px) {
  .bd-placeholder-img-lg {
    font-size: 3.5rem;
  }
}

table, th, td {
border: 1px solid black;
}

</style>
    <!-- Custom styles for this template -->
    <link href="./Blog Template · Bootstrap_files/css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="./Blog Template · Bootstrap_files/blog.css" rel="stylesheet">
  </head>
  <body>
    <div class="container">
  <header class="blog-header py-3">
    <div class="row flex-nowrap justify-content-between align-items-center">
      <div class="col-4 pt-1">
      </div>
      <div class="col-4 text-center">
        <a class="blog-header-logo text-dark" href="index.php">E-Sports TIMES</a>
      </div>
      <div class="col-4 d-flex justify-content-end align-items-center">
        <a class="text-muted" href="   ">
        </a>
      </div>
    </div>
  </header>

  <div class="jumbotron p-4 p-md-5 text-white rounded bg-dark">
    <div class="col-md-6 px-0">
      <h1 class="display-4 font-italic">FaZe Clan</h1>
      <p class="lead my-3">O FaZe Clan é uma organização americana de esports profissional, conhecida principalmente por sua presença no cenário de Call of Duty. Atualmente, eles atuam em equipes de Call of Duty, Counter-Strike: Global Offensive, PLAYERUNKNOWN'S BATTLEGROUNDS, Rainbow Six Siege de Tom Clancy, Rainbow Six, FIFA e Fortnite Battle Royale.</p>
    </div>
  </div>

  <div class="row mb-2">

    <div class="col-md-6">
      <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
        <div class="col p-4 d-flex flex-column position-static">
          <h3 class="mb-0">Marcelo (coldzera)</h3>
          <p class="card-text mb-auto">É um profissional brasileiro em Counter-Strike: Global Offensive. Ele é conhecido como um dos melhores jogadores de todos os tempos no CS:GO, tendo sido classificado #1 pela HLTV por dois anos consecutivos.</p>
        </div>
        <div class="col-auto d-none d-lg-block">
          <img height="250px" width="250px" src="https://pbs.twimg.com/profile_images/1177072610338856960/5wOoqPqF_400x400.jpg">
        </div>
      </div>
    </div>

    <div class="col-md-6">
      <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
        <div class="col p-4 d-flex flex-column position-static">
          <h3 class="mb-0">Nikola (NiKo)</h3>
          <p class="mb-auto">Nikola "NiKo" Kovač (nascido em 16 de fevereiro de 1997) é um profissional de Counter-Strike da Bósnia : Ofensiva Global e ex - jogador de Counter-Strike 1.6.</p>
        </div>
        <div class="col-auto d-none d-lg-block">
          <img height="250px" width="250px" src="https://static.hltv.org/images/playerprofile/thumb/3741/800.jpeg?v=10">
        </div>
      </div>
    </div>

  </div>

<main role="main" class="container">
  <div class="row">
    <div class="col-md-8 blog-main">
      <h3 class="pb-4 mb-4 font-italic border-bottom">
        #GOFAZE
      </h3>

      <div class="blog-post">
        <h2 class="blog-post-title">HISTORIA</h2>

        <hr>
        <p>O FaZe Clan é uma organização americana de esports profissional, conhecida principalmente por sua presença no cenário de Call of Duty. Atualmente, eles atuam em equipes de Call of Duty, Counter-Strike: Global Offensive, PLAYERUNKNOWN'S BATTLEGROUNDS, Rainbow Six Siege de Tom Clancy, Rainbow Six, FIFA e Fortnite Battle Royale. Inicialmente, era uma equipe de sniping no YouTube com vários vídeos de sniping, semelhantes a outra organização americana OpTic Gaming. Eles entraram em jogo competitivo em Call of Duty: Modern Warfare 3 e competiram em todos os títulos de Call of Duty desde então. Em 2016, o FaZe Clan se expandiu para outros jogos competitivos de tiro em primeira pessoa: Counter-Strike: Global Offensive e, mais tarde no verão, Overwatch. O FaZe Clan e seus membros combinados têm os maiores seguidores no YouTube, Twitter, Twitch e Instagram em qualquer organização de esports.</p>
        <blockquote>
          
        <h2>Questões Legais</h2>
        <p>Em maio de 2019, o membro do FaZe, Turner 'Tfue' Tenney, entrou com uma ação no FaZe, alegando que o grupo o havia privado de oportunidades de negócios, não pagou sua parte do dinheiro do patrocínio e levou até 80% de seus ganhos. Tenney também afirmou que alguns membros o encorajaram a jogar e beber enquanto menor de idade. O FaZe divulgou um comunicado negando as alegações e afirmando que não coletou dinheiro com base nos ganhos em torneios de Tenney ou nas receitas de mídia social. Mais tarde, Tenney se retratou da alegação sobre jogos e bebidas menores de idade.</p>

      <div class="blog-post">
        <h2 class="blog-post-title"> </h2>
      </div><!-- /.blog-post -->

</main><!-- /.container -->

<?php include 'footer.php' ?>

</body></html>